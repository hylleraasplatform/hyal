.. hyal documentation master file, created by
   sphinx-quickstart on Thu Oct 12 15:42:18 2023.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

.. image:: /img/hyal-logo-head_dark.png
   :align: right
   :height: 75px


HyAL documentation
================================

:Release:
   |release|
:Date:
   |today|


**Hylleraas Active Learning** (HyAL) is a flexible Python package for active learning framework of
machine learning molecular force-fields. It is built on top of the `Hylleraas Software
Platform`_  (HSP). Through HSP, HyAL is designed to allow interchangable usage
of a range of different machine learning libraries, molecular dynamics engines,
and quantum chemical software.

.. Important::

   The HyAL project is licensed under `The MIT License <https://opensource.org/license/mit/>`_.

   If you use HyAL or any parts of the HyAL code in your publications, please cite the following publications:

      - :fa:`file-text` <placeholder ...>
      - :fa:`file-text` <placeholder ...>
      - :fa:`file-text` <placeholder ...>


.. _`Hylleraas Software Platform`:
   https://hylleraas.readthedocs.io/en/latest/

Installing HyAL
===============
The easiest approach is to install using pip_:

.. code-block:: bash

   python3 -m pip install  git+https://gitlab.com/hylleraasplatform/hyal.git

For more information and **required dependencies**, see
:ref:`installation-label`.

.. _pip:
   http://www.pip-installer.org/en/latest/index.html


Source Code
===========
**Source code** is available from
https://gitlab.com/hylleraasplatform/hyal under the `The MIT License`_. Obtain the source code with `git`_:

.. code-block:: bash

   git clone https://gitlab.com/hylleraasplatform/hyal.git

.. _The MIT License:
   https://opensource.org/license/mit/
.. _git:
   https://git-scm.com/


Development
===========
HyMD is developed and maintained by researchers at the `Hylleraas Centre for
Quantum Molecular Sciences`_ at the `University of Oslo`_.

|pic1| |pic2|

.. _`Hylleraas Centre for Quantum Molecular Sciences`:
   https://www.mn.uio.no/hylleraas/english/
.. _`University of Oslo`:
   https://www.uio.no/

.. |pic1| image:: img/hylleraas_centre_logo_black.png
   :target: img/hylleraas_centre_logo_black.png
   :width: 250 px

.. |pic2| image:: img/uio_full_logo_eng_pos.png
   :target: img/uio_full_logo_eng_pos.png
   :width: 325 px


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

.. toctree::
   :maxdepth: 2
   :caption: Getting started:

   installation.md
   quickstart.md

.. toctree::
   :maxdepth: 2
   :caption: Examples:

   examples.md

.. toctree::
   :maxdepth: 2
   :caption: Developer documentation:

   api.rst
