# Configuration file for the Sphinx documentation builder.
#
# This file only contains a selection of the most common options. For a full
# list see the documentation:
# https://www.sphinx-doc.org/en/master/usage/configuration.html

# -- Path setup --------------------------------------------------------------

# If extensions (or modules to document with autodoc) are in another directory,
# add these directories to sys.path here. If the directory is relative to the
# documentation root, use os.path.abspath to make it absolute, like shown here.
#
# import os
# import sys
# sys.path.insert(0, os.path.abspath('.'))


# -- Project information -----------------------------------------------------

project = 'HyAL'
copyright = '2023, Sigbjørn Løland Bore'
author = 'Sigbjørn Løland Bore, Tilmann Bodenstein'

# The full version, including alpha/beta/rc tags
release = '0.0.x'

github_url = 'https://gitlab.com/hylleraasplatform/hyal'

# -- General configuration ---------------------------------------------------

# Add any Sphinx extension module names here, as strings. They can be
# extensions coming with Sphinx (named 'sphinx.ext.*') or your custom
# ones.
extensions = [
    'sphinx.ext.autodoc',
    'sphinx.ext.autosummary',
    'sphinx.ext.viewcode',
    'sphinx.ext.mathjax',
    'sphinx_rtd_theme',
    'sphinx_tabs.tabs',
    'sphinx_panels',
    'sphinx.ext.intersphinx',
    'numpydoc',
    'myst_parser',
]

# Add any paths that contain templates here, relative to this directory.
templates_path = ['_templates']

# List of patterns, relative to source directory, that match files and
# directories to ignore when looking for source files.
# This pattern also affects html_static_path and html_extra_path.
exclude_patterns = ['_build', 'Thumbs.db', '.DS_Store']


# -- Options for HTML output -------------------------------------------------

# The theme to use for HTML and HTML Help pages.  See the documentation for
# a list of builtin themes.
#
html_theme = 'sphinx_rtd_theme'

html_theme_options = {
    'logo_only': True,
    'display_version': True,
    'collapse_navigation': False,
}

# -- Logos -------------------------------------------------------------------

# html_logo = 'img/hyal-logo-name.png'
# html_favicon = 'img/hyal-logo-head.png'
# html_show_sphinx = False

# html_logo = 'img/hyal-logo-name_black.png'
# html_favicon = 'img/hyal-logo-head_black.png'
# html_show_sphinx = False

html_logo = 'img/hyal-logo-name_dark.png'
html_favicon = 'img/hyal-logo-head_dark.png'
html_show_sphinx = False


# -- Custom CSS and HTML -----------------------------------------------------

# Add any paths that contain custom static files (such as style sheets) here,
# relative to this directory. They are copied after the builtin static files,
# so a file named "default.css" will overwrite the builtin "default.css".
html_static_path = ['_static']
html_css_files = ['custom.css']

html_context = {
    'display_gitlab': True,
    'gitlab_repo': 'hylleraasplatform/hyal',
    'sidebar_external_links_caption': 'Links',
    'sidebar_external_links': [
        (
            '<i class="fa fa-cube fa-fw"></i> PyPI',
            'about:blank',
        ),
        (
            '<i class="fa fa-code fa-fw"></i> Source code',
            'https://gitlab.com/hylleraasplatform/hyal',
        ),
        (
            '<i class="fa fa-bug fa-fw"></i> Issue tracker',
            'https://gitlab.com/hylleraasplatform/hyal/-/issues',
        ),
        (
            '<i class="fa fa-twitter fa-fw"></i> Twitter',
            'https://twitter.com/sigbjornbore',
        ),
        (
            '<i class="fa fa-file-text fa-fw"></i> Citation',
            'about:blank',
        ),
    ],
}


html_sidebars = {
    '**': [
        'versions.html',
    ],
}
